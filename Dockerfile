FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/Paris

ENV PATH="/root/.local/bin:$PATH"

# hadolint ignore=DL3008,DL3013
RUN apt-get update -qy && \
    apt-get install -qy --no-install-recommends \
        software-properties-common curl gnupg2 && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update -qy && \ 
    apt-get install -qy --no-install-recommends  \
        python3.9 \
        python3.9-distutils \
        python3.10 \
        python3.11 \
        # python3.9-dev \
        # python3.10-dev \
        # python3.11-dev \
        # python3.9-venv \
        # python3.10-venv \
        # python3.11-venv
        python3-pip && \
    python3.9 -m pip install --no-cache-dir --upgrade pip && \
    python3.10 -m pip install --no-cache-dir --upgrade pip && \
    python3.11 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir 'poetry==1.6.1' && \
    apt-get clean && \
    rm -rf /var/cache/apt/lists
